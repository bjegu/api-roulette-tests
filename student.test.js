const http = require('http');
const rp = require('request-promise');
const apiUrl = 'https://roulette-api.nowakowski-arkadiusz.com';

// Helper to make the code shorter
const post = (path, hashname = '', body = {}) => rp({
    method: 'POST',
    uri: apiUrl + path,
    body: body,
    json: true,
    headers: { 'Authorization': hashname }
});

// Helper to make the code shorter
const get = (path, hashname = '') => rp({
    method: 'GET',
    uri: apiUrl + path,
    json: true,
    headers: { 'Authorization': hashname }
});

//test('Type your tests here', () => {});

test('Player should have 100 chips', () => {
    let hashname;
    return post('/players')
    .then(response => {
        hashname = response.hashname;
        return get('/chips', hashname);
    })
    .then(response => expect(response.chips).toEqual(100))
});

for (let number of [2,4,6,8,10,11,13,15,17,19,20,22,24,26,29,31,33,35]) {
    test('Black should double the number of chips if number ' + number + ' is spinned', () => {
        return post('/players')
        .then(response => {
            hashname = response.hashname;
            return post('/bets/black', hashname, { chips: 100 }); // a bet
        })
        .then(response => post('/spin/' + number, hashname)) // splin the wheel
        .then(response => get('/chips', hashname)) // checking the number of chips
        .then(response => expect(response.chips).toEqual(200))
    });
}

for (let number of [1,3,5,7,9,12,14,16,18,21,23,25,27,28,30,32,34,36]) {
    test('Red should double the number of chips if number ' + number + ' is spinned', () => {
        return post('/players')
        .then(response => {
            hashname = response.hashname;
            return post('/bets/red', hashname, { chips: 100 }); // a bet
        })
        .then(response => post('/spin/' + number, hashname)) // splin the wheel
        .then(response => get('/chips', hashname)) // checking the number of chips
        .then(response => expect(response.chips).toEqual(200))
    });
}

test.each([1,3,5,7,9,11,13,15,17,19,21,23,25,27,29,31,33,35])('Bet on odds should double the number of chips if number %d is spinned', (number) => {
    return post('/players')
    .then(response => {
        hashname = response.hashname;
        return post('/bets/odd', hashname, { chips: 100 }); // a bet
    })
    .then(response => post('/spin/' + number, hashname)) // splin the wheel
    .then(response => get('/chips', hashname)) // checking the number of chips
    .then(response => expect(response.chips).toEqual(200))
});

test.each([2,4,6,8,10,12,14,16,18,20,22,24,26,28,30,32,34,36])('Bet on even should double the number of chips if number %d is spinned', (number) => {
    return post('/players')
    .then(response => {
        hashname = response.hashname;
        return post('/bets/even', hashname, { chips: 100 }); // a bet
    })
    .then(response => post('/spin/' + number, hashname)) // splin the wheel
    .then(response => get('/chips', hashname)) // checking the number of chips
    .then(response => expect(response.chips).toEqual(200))
});

for (let number of [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18]) {
    test('Low numbers should double the number of chips if number ' + number + ' is spinned', () => {
        return post('/players')
        .then(response => {
            hashname = response.hashname;
            return post('/bets/low', hashname, { chips: 100 }); // a bet
        })
        .then(response => post('/spin/' + number, hashname)) // splin the wheel
        .then(response => get('/chips', hashname)) // checking the number of chips
        .then(response => expect(response.chips).toEqual(200))
    });
}

for (let number of [19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36]) {
    test('High numbers should double the number of chips if number ' + number + ' is spinned', () => {
        return post('/players')
        .then(response => {
            hashname = response.hashname;
            return post('/bets/high', hashname, { chips: 100 }); // a bet
        })
        .then(response => post('/spin/' + number, hashname)) // splin the wheel
        .then(response => get('/chips', hashname)) // checking the number of chips
        .then(response => expect(response.chips).toEqual(200))
    });
}

for (let number of [1,4,7,10,13,16,19,22,25,28,31,34]) {
    test('Bet for column 1 should triple the number of chips if number ' + number + ' is spinned', () => {
        return post('/players')
        .then(response => {
            hashname = response.hashname;
            return post('/bets/column/1', hashname, { chips: 100 }); // a bet
        })
        .then(response => post('/spin/' + number, hashname)) // splin the wheel
        .then(response => get('/chips', hashname)) // checking the number of chips
        .then(response => expect(response.chips).toEqual(300))
    });
}

for (let number of [2,5,8,11,14,17,20,23,26,29,32,35]) {
    test('Bet for column 2 should triple the number of chips if number ' + number + ' is spinned', () => {
        return post('/players')
        .then(response => {
            hashname = response.hashname;
            return post('/bets/column/2', hashname, { chips: 100 }); // a bet
        })
        .then(response => post('/spin/' + number, hashname)) // splin the wheel
        .then(response => get('/chips', hashname)) // checking the number of chips
        .then(response => expect(response.chips).toEqual(300))
    });
}

for (let number of [3,6,9,12,15,18,21,24,27,30,33,36]) {
    test('Bet for column 3 should triple the number of chips if number ' + number + ' is spinned', () => {
        return post('/players')
        .then(response => {
            hashname = response.hashname;
            return post('/bets/column/3', hashname, { chips: 100 }); // a bet
        })
        .then(response => post('/spin/' + number, hashname)) // splin the wheel
        .then(response => get('/chips', hashname)) // checking the number of chips
        .then(response => expect(response.chips).toEqual(300))
    });
}

dozen1 = []
for (let i = 1; i<=12; i++){
    dozen1[i-1] = i
}

for (let number of dozen1) {
    test('Bet for first dozen should triple the number of chips if number ' + number + ' is spinned', () => {
        return post('/players')
        .then(response => {
            hashname = response.hashname;
            return post('/bets/dozen/1', hashname, { chips: 100 }); // a bet
        })
        .then(response => post('/spin/' + number, hashname)) // splin the wheel
        .then(response => get('/chips', hashname)) // checking the number of chips
        .then(response => expect(response.chips).toEqual(300))
    });
}

dozen2 = []
for (let i = 1; i<=12; i++){
    dozen2[i-1] = i+12
}
console.log(dozen2)

for (let number of dozen2) {
    test('Bet for second dozen should triple the number of chips if number ' + number + ' is spinned', () => {
        return post('/players')
        .then(response => {
            hashname = response.hashname;
            return post('/bets/dozen/2', hashname, { chips: 100 }); // a bet
        })
        .then(response => post('/spin/' + number, hashname)) // splin the wheel
        .then(response => get('/chips', hashname)) // checking the number of chips
        .then(response => expect(response.chips).toEqual(300))
    });
}

dozen3 = []
for (let i = 1; i<=12; i++){
    dozen3[i-1] = i+24
}

for (let number of [dozen3]) {
    test('Bet for third dozen should triple the number of chips if number ' + number + ' is spinned', () => {
        return post('/players')
        .then(response => {
            hashname = response.hashname;
            return post('/bets/dozen/3', hashname, { chips: 100 }); // a bet
        })
        .then(response => post('/spin/' + number, hashname)) // splin the wheel
        .then(response => get('/chips', hashname)) // checking the number of chips
        .then(response => expect(response.chips).toEqual(300))
    });
}


for (let number of [0,1,2]) {
    test('Bet for this street should multiply the number of chips by 12 if number ' + number + ' is spinned', () => {
        return post('/players')
        .then(response => {
            hashname = response.hashname;
            return post('/bets/street/0-1-2', hashname, { chips: 100 }); // a bet
        })
        .then(response => post('/spin/' + number, hashname)) // splin the wheel
        .then(response => get('/chips', hashname)) // checking the number of chips
        .then(response => expect(response.chips).toEqual(1200))
    });
}

for (let number of [0,2,3]) {
    test('Bet for this street should multiply the number of chips by 12 if number ' + number + ' is spinned', () => {
        return post('/players')
        .then(response => {
            hashname = response.hashname;
            return post('/bets/street/0-2-3', hashname, { chips: 100 }); // a bet
        })
        .then(response => post('/spin/' + number, hashname)) // splin the wheel
        .then(response => get('/chips', hashname)) // checking the number of chips
        .then(response => expect(response.chips).toEqual(1200))
    });
}



for(let i = 1; i<=12; i=i+3){
    let sprawdzacz = [i, i+1, i+2]
    let endpoint_name = "/bets/street/"+i+"-"+(i+1)+"-"+(i+2)
    for (let number of sprawdzacz) {
        test('Bet for this street should multiply the number of chips by 12 if number ' + number + ' is spinned', () => {
            return post('/players')
            .then(response => {
                hashname = response.hashname;
                return post(endpoint_name, hashname, { chips: 100 }); // a bet
            })
            .then(response => post('/spin/' + number, hashname)) // splin the wheel
            .then(response => get('/chips', hashname)) // checking the number of chips
            .then(response => expect(response.chips).toEqual(1200))
        });
    }
}

for(let i = 0; i<=36; i++){
    let endpoint_name = "/bets/straight/"+i;
        test('Bet for this straight should multiply the number of chips by 36 if number ' + i + ' is spinned', () => {
            return post('/players')
            .then(response => {
                hashname = response.hashname;
                return post(endpoint_name, hashname, { chips: 100 }); // a bet
            })
            .then(response => post('/spin/' + i, hashname)) // splin the wheel
            .then(response => get('/chips', hashname)) // checking the number of chips
            .then(response => expect(response.chips).toEqual(3600))
        });  
}


for (let number of [0,1,2,3]) {
    test('Bet for this corner should multiple the number of chips by 8 if number ' + number + ' is spinned', () => {
        return post('/players')
        .then(response => {
            hashname = response.hashname;
            return post('/bets/corner/0-1-2-3', hashname, { chips: 100 }); // a bet
        })
        .then(response => post('/spin/' + number, hashname)) // splin the wheel
        .then(response => get('/chips', hashname)) // checking the number of chips
        .then(response => expect(response.chips).toEqual(900))
    });
}

for(let i = 1; i<=31; i=i+3){
    let sprawdzacz = [i, i+1, i+3,i+4];
    let endpoint_name = "/bets/corner/"+i+"-"+(i+1)+"-"+(i+3)+"-"+(i+4);
    console.log(endpoint_name)
    for (let number of sprawdzacz) {
        test('Bet for this corner should multiple the number of chips by 8 if number ' + number + ' is spinned', () => {
            return post('/players')
            .then(response => {
                hashname = response.hashname;
                return post(endpoint_name, hashname, { chips: 100 }); // a bet
            })
            .then(response => post('/spin/' + number, hashname)) // splin the wheel
            .then(response => get('/chips', hashname)) // checking the number of chips
            .then(response => expect(response.chips).toEqual(900))
        });
    }
}

for(let i = 2; i<=32; i=i+3){
    let sprawdzacz = [i, i+1, i+3,i+4];
    let endpoint_name = "/bets/corner/"+i+"-"+(i+1)+"-"+(i+3)+"-"+(i+4);
    // console.log(endpoint_name)
    for (let number of sprawdzacz) {
        test('Bet for this corner should multiple the number of chips by 8 if number ' + number + ' is spinned', () => {
            return post('/players')
            .then(response => {
                hashname = response.hashname;
                return post(endpoint_name, hashname, { chips: 100 }); // a bet
            })
            .then(response => post('/spin/' + number, hashname)) // splin the wheel
            .then(response => get('/chips', hashname)) // checking the number of chips
            .then(response => expect(response.chips).toEqual(900))
        });
    }
}

for(let i = 1; i<=31; i=i+3){
    let sprawdzacz = [i, i+1, i+2,i+3,i+4,i+5];
    let endpoint_name = "/bets/line/"+i+"-"+(i+1)+"-"+(i+2)+"-"+(i+3)+"-"+(i+4)+"-"+(i+5);
    console.log(endpoint_name)
    for (let number of sprawdzacz) {
        test('Bet for this corner should multiple the number of chips by 6 if number ' + number + ' is spinned', () => {
            return post('/players')
            .then(response => {
                hashname = response.hashname;
                return post(endpoint_name, hashname, { chips: 100 }); // a bet
            })
            .then(response => post('/spin/' + number, hashname)) // splin the wheel
            .then(response => get('/chips', hashname)) // checking the number of chips
            .then(response => expect(response.chips).toEqual(600))
        });
    }
}

//splity z 0
for(let i = 1; i<=3; i=i+1){
    let sprawdzacz = [0,i];
    let endpoint_name = "/bets/split/0-"+(i);
    console.log(endpoint_name)
    for (let number of sprawdzacz) {
        test('Bet for this corner should multiple the number of chips by 18 if number ' + number + ' is spinned', () => {
            return post('/players')
            .then(response => {
                hashname = response.hashname;
                return post(endpoint_name, hashname, { chips: 100 }); // a bet
            })
            .then(response => post('/spin/' + number, hashname)) // splin the wheel
            .then(response => get('/chips', hashname)) // checking the number of chips
            .then(response => expect(response.chips).toEqual(1800))
        });
    }
}

//splity co 1 
for(let i = 1; i<=35; i=i+1){
    let sprawdzacz = [i,i+1];
    let endpoint_name = "/bets/split/"+i+"-"+(i+1);
    console.log(endpoint_name)
    for (let number of sprawdzacz) {
        test('Bet for this corner should multiple the number of chips by 18 if number ' + number + ' is spinned', () => {
            return post('/players')
            .then(response => {
                hashname = response.hashname;
                return post(endpoint_name, hashname, { chips: 100 }); // a bet
            })
            .then(response => post('/spin/' + number, hashname)) // splin the wheel
            .then(response => get('/chips', hashname)) // checking the number of chips
            .then(response => expect(response.chips).toEqual(1800))
        });
    }
}

//splity co 1 
for(let i = 1; i<=33; i++){
    let sprawdzacz = [i,i+3];
    let endpoint_name = "/bets/split/"+i+"-"+(i+3);
    console.log(endpoint_name)
    for (let number of sprawdzacz) {
        test('Bet for this corner should multiple the number of chips by 18 if number ' + number + ' is spinned', () => {
            return post('/players')
            .then(response => {
                hashname = response.hashname;
                return post(endpoint_name, hashname, { chips: 100 }); // a bet
            })
            .then(response => post('/spin/' + number, hashname)) // splin the wheel
            .then(response => get('/chips', hashname)) // checking the number of chips
            .then(response => expect(response.chips).toEqual(1800))
        });
    }
}